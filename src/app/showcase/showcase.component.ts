import { Component, OnInit } from '@angular/core';
import { MatDialog, } from '@angular/material/dialog';

import { DialogViewDataComponent } from '../dialog-view-data/dialog-view-data.component';
import { showcaseImages, ShowcaseImage } from '../../data'

@Component({
  selector: 'app-showcase',
  templateUrl: './showcase.component.html',
  styleUrls: ['./showcase.component.scss']
})
export class ShowcaseComponent implements OnInit {

  showcases:ShowcaseImage[]

  constructor(
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.showcases = showcaseImages
    // this.showcases = showcaseImages.map(project => {
    //   project.background = 'assets/images/' + project.project + '/' + project.background
    //   const images =  project.images.map(image => {
    //     return {
    //       path: `assets/images/${project.project + '/' + image.path}`
    //     }
    //   })
    //   return {
    //       project: project.project,
    //       background: project.background,
    //       images
    //   }
    // })
    
  }

  openDialog(data: ShowcaseImage) {


    this.dialog.open(DialogViewDataComponent, {
      data,
      width: '800px',
      height: '900px'
    });
  }

}

