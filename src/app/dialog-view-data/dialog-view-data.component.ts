import { Component, OnInit,Inject } from '@angular/core';
import { MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ShowcaseImage } from '../../data';


@Component({
  selector: 'app-dialog-view-data',
  templateUrl: './dialog-view-data.component.html',
  styleUrls: ['./dialog-view-data.component.scss']
})
export class DialogViewDataComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: ShowcaseImage
    ) {}

  ngOnInit() {
  }

}
