import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogViewDataComponent } from './dialog-view-data.component';

describe('DialogViewDataComponent', () => {
  let component: DialogViewDataComponent;
  let fixture: ComponentFixture<DialogViewDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogViewDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogViewDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
