import { Component, OnInit, Input } from '@angular/core';
import { GalleryItem, ImageItem,Gallery } from '@ngx-gallery/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { ShowcaseImage } from '../../data';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  constructor(
    public gallery: Gallery,
    public breakpointObserver: BreakpointObserver,
    ) { }

  @Input() showcase: ShowcaseImage;


  galleryId = 'myLightbox';
  images: GalleryItem[];
  
  col = 3
  rowHeight = 'fit'

  ngOnInit() {
    
    if (this.breakpointObserver.isMatched('(max-width: 769px)')) {
      this.col = 1
      this.rowHeight = '100px'
    }

    this.images = this.showcase.images.map((img)=> {
      return new ImageItem({ src:img.path, thumb: img.path})
    })

    
    const galleryRef = this.gallery.ref(this.galleryId);
    // galleryRef.setConfig({loadingMode: 'indeterminate', loadingStrategy: 'lazy'});
    galleryRef.load(this.images);


  }

}
