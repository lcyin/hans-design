import { Component, OnInit,Inject } from '@angular/core';
import {MatDialog, } from '@angular/material/dialog';
import { DialogViewDataComponent } from '../dialog-view-data/dialog-view-data.component';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    public dialog: MatDialog, 
    ) { }

 
  ngOnInit() {

  }

  
  openDialog(image:string) {
    this.dialog.open(DialogViewDataComponent, {
      data: {
        image
      }
    });
  }


}
