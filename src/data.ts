

export interface Image {
    path: string
}

export interface ShowcaseImage {
    project:string,
    background:string,
    images : Image[]
}



export const showcaseImages: ShowcaseImage[] = [
    {
        project: 'FONTANA GARDENS',
        background: 'assets/images/FONTANA GARDENS/daugther_bathroom_13062019.jpeg',
        images: [
            {
                path: 'assets/images/FONTANA GARDENS/daugther_bathroom_13062019.jpeg'
            },
            {
                path: 'assets/images/FONTANA GARDENS/daugther_bedromm_2_15052019.jpeg'
            },
            {
                path: 'assets/images/FONTANA GARDENS/daugther_bedroom_1_15052019.jpeg'
            },
            {
                path: 'assets/images/FONTANA GARDENS/dinning_area_view1_23052019.jpg'
            },
            {
                path: 'assets/images/FONTANA GARDENS/Entrance_view_1_23052019.jpg'
            },
            {
                path: 'assets/images/FONTANA GARDENS/fish_tank_23052019.jpg'
            },
            {
                path: 'assets/images/FONTANA GARDENS/guest_bathroom.jpeg'
            },
            {
                path: 'assets/images/FONTANA GARDENS/guest_bathroom_16062019.jpeg'
            },
            {
                path: 'assets/images/FONTANA GARDENS/guestroom.jpeg'
            },
            {
                path: 'assets/images/FONTANA GARDENS/gym_area_view_1_23052019.jpg'
            },
            {
                path: 'assets/images/FONTANA GARDENS/gym_area_view_2_23052019.jpg'
            },
            {
                path: 'assets/images/FONTANA GARDENS/living_area_view_1_23052019.jpg'
            },
            {
                path: 'assets/images/FONTANA GARDENS/living_area_view_2_23052019.jpg'
            },
            {
                path: 'assets/images/FONTANA GARDENS/living_room_view_5_13062019.jpg'
            },
            {
                path: 'assets/images/FONTANA GARDENS/master_bathroom_1.jpeg'
            },
            {
                path: 'assets/images/FONTANA GARDENS/master_bathroom_2.jpeg'
            },
            {
                path: 'assets/images/FONTANA GARDENS/MASTER_BEDROOM_VIEW_1_13062019.jpeg'
            },
            {
                path: 'assets/images/FONTANA GARDENS/MASTER_BEDROOM_VIEW_2_15052019.jpg'
            },
        ]
    },
    {
        project: 'IMPERIAL CULLINAN',
        background: 'assets/images/Imperial Cullinan/4d89d736-67d7-47ea-ac16-3837f9530d74.jpg',

        images: [
            {
                path: 'assets/images/Imperial Cullinan/4d89d736-67d7-47ea-ac16-3837f9530d74.jpg'
            },
            {
                path: 'assets/images/Imperial Cullinan/6a1a0a0e-61f5-471d-b717-68c1aac2018a.jpg'
            },
            {
                path: 'assets/images/Imperial Cullinan/65a89dab-39d0-4e49-b50a-90518f25d625.jpg'
            },
            {
                path: 'assets/images/Imperial Cullinan/90ad4014-8b47-4fcf-acd0-2c2595778cd4.jpg'
            },
            {
                path: 'assets/images/Imperial Cullinan/557eba5c-5559-45b9-9fb2-9535ffcefdcc.jpg'
            },
            {
                path: 'assets/images/Imperial Cullinan/82953087-01d1-494c-b13b-e88a7a04038f.jpg'
            },
            {
                path: 'assets/images/Imperial Cullinan/b15fb4e6-3455-44c0-8aed-2926a5f15d6b.jpg'
            },
            {
                path: 'assets/images/Imperial Cullinan/b4050ff0-870b-4371-bdf8-d22533e1bced.jpg'
            },
            {
                path: 'assets/images/Imperial Cullinan/e3f2f014-f756-407e-8856-7043739a61a0.jpg'
            },
            {
                path: 'assets/images/Imperial Cullinan/e8fcc156-7294-45d0-a789-c57d3af47629.jpg'
            },

        ]
    },
    {
        project: 'MOUNTAIN COURT',
        background: 'assets/images/MOUNTAIN COURT/054ac1e3-80fd-42d3-8303-869a30652cec.jpg',

        images:[
            {
                path: 'assets/images/MOUNTAIN COURT/054ac1e3-80fd-42d3-8303-869a30652cec.jpg'
            },
            {
                path: 'assets/images/MOUNTAIN COURT/acc43707-4cc0-47a4-95e4-05cd124de4c0.jpg'
            },
            {
                path: 'assets/images/MOUNTAIN COURT/bathroom_1.jpeg'
            },
            {
                path: 'assets/images/MOUNTAIN COURT/bathroom_2.jpeg'
            },
            {
                path: 'assets/images/MOUNTAIN COURT/bedroom_1.jpeg'
            },
            {
                path: 'assets/images/MOUNTAIN COURT/bedroom_2.jpeg'
            },
            {
                path: 'assets/images/MOUNTAIN COURT/bedroom_3.jpeg'
            },
            {
                path: 'assets/images/MOUNTAIN COURT/bedroom_4.jpeg'
            },
            {
                path: 'assets/images/MOUNTAIN COURT/c6d5a6de-1c69-43c8-9765-1292a49a6e2f.jpg'
            },
            {
                path: 'assets/images/MOUNTAIN COURT/clothing_room_1.jpeg'
            },
            {
                path: 'assets/images/MOUNTAIN COURT/entertainment_room_1.jpeg'
            },
            {
                path: 'assets/images/MOUNTAIN COURT/work_area.jpeg'
            }
        ]
    },
    {
        project: 'NAPA',
        background: 'assets/images/NAPA/0908_gymroom01.jpg',

        images:[
            {
                path: 'assets/images/NAPA/0908_gymroom01.jpg'
            },
            {
                path: 'assets/images/NAPA/0925_bedroom3_01.jpg'
            },
            {
                path: 'assets/images/NAPA/1021_01.jpg'
            },
            {
                path: 'assets/images/NAPA/1026_bedroom1_01.jpg'
            },
            {
                path: 'assets/images/NAPA/1027_bedroom2_01.jpg'
            },
            {
                path: 'assets/images/NAPA/guest_room.jpeg'
            },
            {
                path: 'assets/images/NAPA/pool_side.jpg'
            }
        ]
    },
    {
        project: 'POKFULAM PEAK',
        background: 'assets/images/Pokfulam Peak/1f_BACK_YARD_VIEW1_30042019_v2.jpg',

        images: [
            {
                path: 'assets/images/Pokfulam Peak/1f_BACK_YARD_VIEW1_30042019_v2.jpg'
            },
            {
                path: 'assets/images/Pokfulam Peak/1F_DINNING_AREA_23042019_V2.jpg'
            },
            {
                path: 'assets/images/Pokfulam Peak/1f_FRONT_YARD_VIEW1_30042019_v2.jpg'
            },
            {
                path: 'assets/images/Pokfulam Peak/1f_FRONT_YARD_VIEW2_30042019_v2.jpg'
            },
            {
                path: 'assets/images/Pokfulam Peak/1F_LIVING_AREA_23042019_V2.jpg'
            },
            {
                path: 'assets/images/Pokfulam Peak/1F_LIVING_AREA_VIEW_2_23042019_V2.jpg'
            },
            {
                path: 'assets/images/Pokfulam Peak/2F_GAME_ROOM_23042019.jpeg'
            },
            {
                path: 'assets/images/Pokfulam Peak/3F_MASTER_BATHROOM_VIEW_1_30042019.jpg'
            },
            {
                path: 'assets/images/Pokfulam Peak/3F_MASTER_BEDROOM_VIEW_2_27042019.jpeg'
            },
            {
                path: 'assets/images/Pokfulam Peak/3F_MASTER_BEDROOM_VIEW1_27042019.jpeg'
            },
            {
                path: 'assets/images/Pokfulam Peak/3F_MASTER_BEDROOM_VIEW3_27042019.jpeg'
            },
            {
                path: 'assets/images/Pokfulam Peak/BEDROOM_1_VIEW1_29042019.jpeg'
            },
            {
                path: 'assets/images/Pokfulam Peak/BEDROOM_1_VIEW2_29042019.jpeg'
            },
            {
                path: 'assets/images/Pokfulam Peak/KID_ROOM_VIEW1_02052019.jpeg'
            },
            {
                path: 'assets/images/Pokfulam Peak/KID_ROOM_VIEW2_020502019.jpeg'
            },
            {
                path: 'assets/images/Pokfulam Peak/RF_JACCUZI_ARAE_27042019.jpg'
            },
            {
                path: 'assets/images/Pokfulam Peak/RF_YARD_27042019.jpg'
            }
        ]
    },
    {
        project: 'PRIVATE PROJECT',
        background: 'assets/images/PRIVATE PROJECT/81645792_2383627425301141_4380609493638053888_o.jpg',

        images: [
            {
                path: 'assets/images/PRIVATE PROJECT/81645792_2383627425301141_4380609493638053888_o.jpg'
            },
            {
                path: 'assets/images/PRIVATE PROJECT/81699569_2383627871967763_6984830585348816896_o.jpg'
            },
            {
                path: 'assets/images/PRIVATE PROJECT/81721218_2383629798634237_2110726441707503616_o.jpg'
            },
            {
                path: 'assets/images/PRIVATE PROJECT/81979743_2383641505299733_8046248773787058176_o.jpg'
            },
            {
                path: 'assets/images/PRIVATE PROJECT/81998681_2383641631966387_2669146239718653952_o.jpg'
            },
            {
                path: 'assets/images/PRIVATE PROJECT/82079233_2383629985300885_2524341133866172416_o.jpg'
            },
            {
                path: 'assets/images/PRIVATE PROJECT/82323100_2383641318633085_3415792030289231872_o.jpg'
            }
        ]
    },
    {
        project: 'THE PEAK',
        background: 'assets/images/The Peak/0606_bedroom01.jpg',

        images: [
            { 
                path: 'assets/images/The Peak/0606_bedroom01.jpg'
            },
            { 
                path: 'assets/images/The Peak/0621_2fkidsroom02.jpg'
            },
            { 
                path: 'assets/images/The Peak/0629_2fkidsroom01.jpg'
            },
            { 
                path: 'assets/images/The Peak/0731_2fkidsroom02.jpg'
            },
            { 
                path: 'assets/images/The Peak/0821_msbedroom01.jpg'
            },
            { 
                path: 'assets/images/The Peak/0822_2fkidsroom01.jpg'
            },
            { 
                path: 'assets/images/The Peak/0829_1fbath01.jpg'
            },
            { 
                path: 'assets/images/The Peak/0829_1fwashroom01.jpg'
            },
            { 
                path: 'assets/images/The Peak/0829_12bathroom01.jpg'
            },
            { 
                path: 'assets/images/The Peak/0829_bedroom01.jpg'
            },
            { 
                path: 'assets/images/The Peak/0829_bookshelf01.jpg'
            },
            { 
                path: 'assets/images/The Peak/0829_bookshelf02.jpg'
            },
            { 
                path: 'assets/images/The Peak/0829_ug01.jpg'
            },
            { 
                path: 'assets/images/The Peak/0830_living01.jpg'
            },
            { 
                path: 'assets/images//0830_living02.jpg'
            },
            { 
                path: 'assets/images/The Peak/0915_frontgarden01.jpg'
            },
            { 
                path: 'assets/images/The Peak/0915_gfbath01.jpg'
            },
            { 
                path: 'assets/images/The Peak/0915_gfbath02.jpg'
            },
            { 
                path: 'assets/images/The Peak/20180829_dining01.jpg'
            },
            { 
                path: 'assets/images/The Peak/20180829_kitchen01.jpg'
            },
            { 
                path: 'assets/images/The Peak/20180830_backgarden01.jpg'
            }
        ]
    }
]